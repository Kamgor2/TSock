import { createServer as createSecureServer, Server as SecureServer } from 'https';
import { IListenOptions } from './IListenOptions';

export class Application {
	private static _instance: Application;
	private mainModule;

	private constructor() {}

	public static appFactory(mainModule?): Application {
		if (Application._instance !== null && Application._instance !== undefined) {
			return Application._instance;
		} else {
			if (!mainModule) {
				throw new Error('Main module not set!');
			}
			const instance = new this();
			instance.mainModule = mainModule;
			return instance;
		}
	}

	public listen(listenOptions: IListenOptions = {}) {
		let { port } = listenOptions;
		const { secure } = listenOptions;
		if (!port) {
			port = 80;
		}
		if (!port && secure) {
			port = 443;
		}
	}

}
