export interface IListenOptions {
    port?: number;
    secure?: boolean;
}