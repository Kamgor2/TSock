import { Scope } from './ScopeEnum';

export interface IInjectableOptions {
    scope?: Scope;
    asynchronous?: boolean;
}