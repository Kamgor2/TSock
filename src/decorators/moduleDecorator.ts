import { IModuleBase } from '../model/IModuleBase';
import { IModuleOptions } from './IModuleOptions';

export const Module = (moduleOptions: IModuleOptions) => {
	return <any> ((target: IModuleBase) => {
		target.__import = moduleOptions.import;
		target.__export = moduleOptions.export;
		target.__controllers = moduleOptions.controllers;
		target.__services = moduleOptions.services;
		return class extends target {
			protected parent: any;

			constructor() {
				super();
			}
		};
	});
};
