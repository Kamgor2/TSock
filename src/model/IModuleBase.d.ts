import { IControllerBase } from './IControllerBase';
import { IInjectableBase } from './IInjectableBase';

export interface IModuleBase {
    new ();
    __import: IModuleBase[];
    __export: IModuleBase[];
    __controllers: IControllerBase[];
    __services: IInjectableBase[];
}