import { IControllerOptions } from '../decorators/IControllerOptions';
import { IInjectableBase } from './IInjectableBase';

export interface IControllerBase {
    new (...services: IInjectableBase[]);
    __namespace: string;
    __neededServices: IInjectableBase[];
}