export interface IMessageHandler {
    (messageType: string, message: any): any;
}