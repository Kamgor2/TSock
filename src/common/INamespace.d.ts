import { IMessageHandler } from "./IMessageHandler";

export interface INamespace {
    name: string;
    parent?: INamespace;
    children?: INamespace[];
    messageHandlers: IMessageHandler[];
}