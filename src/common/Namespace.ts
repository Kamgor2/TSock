import { INamespace } from './INamespace';

export class Namespace implements INamespace {
	constructor(
		public name: string,
		public parent?: Namespace,
		public children?: Namespace[]
	) {}
}
