import { Application } from '../../Application';
import { MainModule } from './mainModule.module';

const app = Application.appFactory({
	mainModule: MainModule
});

app.listen({
	port: 8080
});
