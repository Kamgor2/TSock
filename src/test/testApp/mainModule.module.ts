import { Module } from '../../decorators/module.decorator';
import { MainController } from './mainController.controller';

@Module({
	controllers: [
		MainController
	]
})
export class MainModule {

}
